package com.cardenas.probandointent;



import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

public class IntentActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnWeb,btnTel,btnSMS,btnMap,btnOff;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Ciclo de vida","OnCreate");
        setContentView(R.layout.activity_intent);
        btnWeb = (Button)findViewById(R.id.btnWeb);
        btnTel= (Button)findViewById(R.id.btnTel);
        btnSMS= (Button)findViewById(R.id.btnSMS);
        btnMap= (Button)findViewById(R.id.btnMap);
        btnWeb.setOnClickListener(this);
        btnTel.setOnClickListener(this);
        btnSMS.setOnClickListener(this);
        btnMap.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Ciclo de vida","onResume");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("Ciclo de vida","onStart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Ciclo de vida","onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("Ciclo de vida","onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("Ciclo de vida","onDestroy");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("Ciclo de vida","onRestart");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnWeb:
                String url = "http://www.google.com";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;
            case R.id.btnSMS:
                String number = "+56981939074";  // The number on which you want to send SMS
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
                break;
            case R.id.btnTel:
                Intent intelCall = new Intent(Intent.ACTION_DIAL,
                Uri.parse("tel:+56981939074"));
                startActivity(intelCall);
                break;
            case R.id.btnMap:
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=restaurants");
                showMap(gmmIntentUri);
                break;
        }
    }
    public void showMap(Uri gmmIntentUri) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(gmmIntentUri);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
